import socket
import threading
import queue
from time import sleep


messagequeue = queue.Queue()
webqueue = queue.Queue()

class Client():
    def __init__(self,conn,addr):
        self.mode = 0  #can be V(Viewer) or W(Writer)
        self.conn = conn
        self.addr = addr
        self.rcvthread = threading.Thread(target=self.handle)
        self.rcvthread.daemon = True
        self.rcvthread.start()
        self.isalive = True
        self.name = ""
        self.queue = queue.Queue()
        self.nameflag = False
    def get_addr(self):
        return self.addr
    def handle(self):
        while 1:
            sleep(0.05)
            try:
                data = self.conn.recv(1024)
            except:
                break
            if data[0:4] == bytes("mode",'UTF-8'):
                if data[4] == 50:
                    self.mode = 2
                elif data[4] == 49:
                    self.mode = 1
                #print("mode is " + str(self.mode))
                if self.mode == 2 :
                    self.sendqueue()
                if not self.nameflag:
                    self.name = data[5:].decode('UTF-8')
                    self.nameflag = True
            elif data[0:4] == bytes("left",'UTF-8'):
                    self.isalive = False
            elif data :
                originb = "<font color='red'><b>"+self.name.upper()+"</b></font> from <b>IP</b>:"+\
                          self.addr[0]+" <b>PORT:</b>"+str(self.addr[1])+\
                          "<b> => </b>"
                d = {'addr':self.addr,'data':data}
                messagequeue.put(d)
                webqueue.put(originb+" <font color='green' size=6>"+data.decode('UTF-8')+"</font>")

    def send(self,data):
        try:
            self.conn.send(data['data'])
        except:
            self.isalive = False
            print(self.name + " is OoOo0ppppppsssssssss kh kh kh")
    def sendqueue(self):
        try:
            while not self.queue.empty():
                 self.conn.send(self.queue.get()['data'])
        except:
            self.isalive = False
            print(self.name + " is OoOo0ppppppsssssssss kh kh kh")
    def addtoqueue(self,data):
        self.queue.put(data)
		
class WebClient():
	def __init__(self,conn,addr,server):
            self.conn = conn
            self.addr = addr
            self.T = threading.Thread(target=self.handle)
            self.T.daemon = True
            self.T.start()
            self.server = server

	def handle(self):
            while True:
                sleep(0.05)
                data = self.conn.recv(1024)
                if data.decode('UTF-8') == "get":
                        self.conn.send(bytes(server.get_message() ,'UTF-8'))

		
	

class AmirServer():
    clients = []
    webclients = []
    def __init__(self, TCP_IP, TCP_PORT_client,TCP_PORT_web, BUFER_SIZE):
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.bind((TCP_IP, TCP_PORT_client))
            self.s.listen(1)
            self.webserver = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.webserver.bind((TCP_IP, TCP_PORT_web))
            self.webserver.listen(1)
            self.flaskacceptorT = threading.Thread(target=self.webhandler)
            self.flaskacceptorT.daemon = True
            self.flaskacceptorT.start()
            self.acceptorT = threading.Thread(target=self.socketacceptor)
            self.acceptorT.daemon = True
            self.acceptorT.start()
            print("server start to listening socket")
            self.handlerT = threading.Thread(target=self.queuehandler)
            self.handlerT.daemon = True
            self.handlerT.start()
            self.deleteT = threading.Thread(target=self.socketdeletor)
            self.deleteT.daemon = True
            self.deleteT.start()
            self.webmessage = ""
    def socketacceptor(self):
        while 1:
            sleep(0.2)
            conn , addr = self.s.accept()
            c = Client(conn,addr)
            print('Connection address:',c.get_addr())
            self.clients.append(c)
    def queuehandler(self):
        while 1:
            sleep(0.05)
            if not messagequeue.empty() :
                data = messagequeue.get()
                for peer in self.clients :
                    if peer.mode == 2:
                            peer.send(data)
                    elif peer.addr != data['addr']:
                        peer.addtoqueue(data)
            self.web()

    def socketdeletor(self):
        while 1:
            sleep(0.1)
            for peer in self.clients :
                if not peer.isalive :
                    self.clients.remove(peer)
                    print(peer.addr," removed from list")

    def web(self):
            if not webqueue.empty():
                self.webmessage += webqueue.get() + "<p>"

    def get_message(self):
        return self.webmessage

    def webhandler(self):
        conn, addr = self.webserver.accept()
        c = WebClient(conn, addr,self)
        print('Connection address:', addr)
        self.webclients.append(c)

		

server = AmirServer('0.0.0.0',5005,2000,1024)

while 1:
    sleep(1)
    pass
