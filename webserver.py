from flask import Flask,request,render_template
import socket
import threading
from time import sleep

app = Flask(__name__)

def connect(TCP_IP,TCP_PORT):
    TCP_IP = TCP_IP
    TCP_PORT = TCP_PORT
    BUFFER_SIZE = 1024
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(1)
    connect = False
    try:
        s.connect((TCP_IP, TCP_PORT))
        print("you are now connected to the server")
        connect = True
    except :
        print("server not found")
        quit()
    return s

def get(socket):
        data = ""
        try:
            socket.send(bytes("get", 'UTF-8'))
            data = socket.recv(1024).decode('UTF-8')
        except:
            pass
        return data


		 
socket = connect(TCP_IP= '0.0.0.0',TCP_PORT=2000)

def run():
    app.run(host='0.0.0.0',port=80)


@app.route("/",methods=['GET', 'POST'])
def index():
#    return("the main page is here")
    if request.method=="POST":
        if request.form.get("usr")=="admin" and request.form.get("pass")=="admin":
            return get(socket)
        else:
            return render_template("login.html")
    else:
        #show the form
        return render_template("login.html")

t = threading.Thread(target=run)
t.daemon = True
t.start()

while 1:
	sleep(1)
